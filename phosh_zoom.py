#!/usr/bin/python3
import time
import os
import subprocess


L5_input = "cat /proc/bus/input/devices | grep -P '^[NH]: ' | paste - - | grep EP0700M09 | cut -d '='  -f3 | cut -d ' ' -f1"
INPUT_DEVICE=f"/dev/input/{os.popen(L5_input).read()}".strip()
debug = print

last_set_dpi = 2
def composit_process(data):
    global last_set_dpi
    #print(data[2])
    dpi_change = pos_to_scale(data[2])

    if dpi_change != 0:
        last_set_dpi = last_set_dpi + dpi_change
        if last_set_dpi > 4:
            last_set_dpi = 4
        if last_set_dpi < 1:
            last_set_dpi = 1
        cmd = f"wlr-randr --output DSI-1 --scale {last_set_dpi}"
        print(cmd)
        os.system(cmd)



def pos_to_scale(val):
    val = int(val)
    if val > 1270:
        val = 1270
    if val < 270:
        val = 270
    
    val = val - 270
    
    #val = 0-1000
    val = val - 500
    #val = -500-500
    val = val/1000
    #val = -.05-.05 
    return(val)

def touch_driver():
    touch_cmd = f"sudo evtest {INPUT_DEVICE}"
    process = subprocess.Popen(touch_cmd, stdout=subprocess.PIPE, shell=True)
    #debug(touch_cmd)
    found_pinch = False
    MT_SLOT = -1
    x = -1
    y = -1
    touching = -1
    
    while True:
        #debug("Touch test")

        
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            output = str(output)
            if "ABS_MT_SLOT" in output:
                MT_SLOT  = output.split("value ")[-1][0]
            if "BTN_TOUCH" in output:
                touching = output.split("value ")[-1]
                touching = touching.split("\\")[0]
                #debug (f"touching: {touching}")
                #check if we just tapped
                if touching == '0':
                    pass
                    #debug(["click", x, y])
                    #composit_process(["click", x, y])
            if "ABS_X" in output:
                temp_x = output.split("value ")[-1]
                temp_x = temp_x.split("\\")[0]
                if temp_x.isdigit():
                    x = temp_x
                #TODO update x
                if touching == '1':
                    pass
                    #debug (["drag", x, y])
                    #composit_process(["drag", x, y])
            if "ABS_Y" in output:
                temp_y = output.split("value ")[-1]
                temp_y = temp_y.split("\\")[0]
                if temp_y.isdigit():
                    y = temp_y
                if touching == '1':
                    pass
                    #debug (["drag", x, y])
                    #composit_process(["drag", x, y])
            if x != -1 and y != -1 and touching != -1 and MT_SLOT != -1:
                #if 3 touch points
                if MT_SLOT == "2":
                    composit_process(["Yo", x, y, MT_SLOT])
                MT_SLOT = -1
                x = -1
                y = -1
                touching = -1
            #else:
                #print (f"stuff: {output}")
            #("move",x,y)
            
    rc = process.poll()


touch_driver()
